# Recico app

_Aplicación web la cual permite fomentar el reciclaje, gamificando el progreso de cada equipo_

## Comenzando 🚀

_Este proyecto fue realizado con ReactJS, Sass, Bootstrap, AWS S3_

### Pre-requisitos 📋

_Instala las dependencias del proyecto_

```
npm install
```

_Inicializa el proyecto de manera local_

```
npm run build:local
```

## Versionado 📌

Usamos [Gitlab](https://gitlab.com/recicoapp/recicoapp-front) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://gitlab.com/recicoapp/recicoapp-front).

## Autores ✒️

_Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios_

- **Kevin Garzón** - _Fullstack developer_ - [LinkedIn](https://www.linkedin.com/in/kevin-garzon-rodriguez-877921137/)
- **Eduard Castellanos** - _Fullstack developer_ - [LinkedIn](https://www.linkedin.com/in/eduard-andres-castellanos-torres-48931597/)
- **Leonardo Gonzalez** - _Fullstack developer_ - [LinkedIn](https://www.linkedin.com/in/leonardo-gonzalez-rod/)
- **David Rojas** - _Fullstack developer_ - [LinkedIn](https://www.linkedin.com/in/deveeup/)

---

⌨️ por B4Tech
