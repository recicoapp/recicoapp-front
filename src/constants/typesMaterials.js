// @ts-check
/**
 * @typedef {{ id: string, label: string, materials: string[] }} TypesMaterial
 * @type {TypesMaterial[]}
 */
export const typesMaterials = [
  { id: 'ret1', label: 'Residuos ordinarios, no reciclables ', materials: [] },
  { id: 'ret2', label: 'Cartón y papel', materials: [] },
  { id: 'ret3', label: 'Plásticos', materials: [] },
  { id: 'ret4', label: 'Vidrio', materials: [] },
  { id: 'ret5', label: 'Residuos orgánicos', materials: [] },
  { id: 'ret6', label: 'Aluminio o metales', materials: [] },
  { id: 'ret7', label: 'Residuos peligrosos', materials: [] },
];
