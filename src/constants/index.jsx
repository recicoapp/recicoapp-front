// @ts-check
import { generatePath } from 'react-router';

import KevinImg from 'assets/img/Kevin.jpeg';
import EduardImg from 'assets/img/Eduard.jpeg';
import LeoImg from 'assets/img/Leo.jpeg';
import DavidImg from 'assets/img/David.jpg';

export const PathRoutes = {
  LOGIN: '/login',
  INDEX: '/',
  MAIN: '/home',
  MEMBERS: '/developers/:dev',
  APP: '/app',
  REGISTER_TOURNAMENT: '/app/newTournament',
  RESULTS: '/results/:id',
  RECORD_RECYCLING_AMOUNT: '/app/record_recycling_amount',
  NO_FOUND: '/:*',
};

export const createRouteMember = (dev) => generatePath(PathRoutes.MEMBERS, { dev });
/**
 * @type {Record<'kevin' | 'eduard' | 'leonardo' | 'david',
 * {
 *   url: string;
 *   name: string;
 *   github: string;
 *   resume: string;
 *   img: string;
 *   linkedin: string;
 *   skills: string[];
 * } >}
 */
export const MEMBERS = {
  kevin: {
    url: createRouteMember('kevin'),
    name: 'Kevin Garzon',
    github: 'https://github.com/Kevingarzon94',
    resume:
      'Desarrollador fullstack, apasionado por IA, cuento con fuertes conocimientos en AWS, apasionado por los servicios que ofrece AWS / Alexa. +4 años desarrollando.',
    img: KevinImg,
    linkedin: 'https://www.linkedin.com/in/kevin-garzon-rodriguez-877921137/',
    skills: ['Fullstack', 'NodeJS', 'AWS', 'Alexa Skills'],
  },
  eduard: {
    url: createRouteMember('eduard'),
    name: 'Eduard Castellanos',
    github: 'https://github.com/EduardMcfly',
    resume:
      'Soy un desarrollador completo de JavaScript y TypeScript con más de 3 años de experiencia,  me gusta la tecnología, actualmente estoy estudiando ingeniería de software, me gusta estudiar y aprender continuamente, me gusta la nube (AWS/Azure) y el desarrollo web',
    img: EduardImg,
    linkedin: 'https://www.linkedin.com/in/eduard-andres-castellanos-torres-48931597/',
    skills: ['Fullstack', 'TypeScript', 'NodeJS', 'AWS'],
  },
  leonardo: {
    url: createRouteMember('leonardo'),
    name: 'Leonardo Gonzalez',
    github: 'https://github.com/ingleo',
    resume:
      'i am a systems engineer, passionate about software development and architecture, based on agile values, looking for a new challenges and knowledge in the cloud',
    img: LeoImg,
    linkedin: 'https://www.linkedin.com/in/leonardo-gonzalez-rod/',
    skills: ['Fullstack', 'Scrum Master', 'NodeJS', 'AWS'],
  },
  david: {
    url: createRouteMember('david'),
    name: 'David Rojas',
    github: 'https://github.com/deveeup',
    resume:
      'Ing. Sistemas - fullstack developer +3 años de experiencia trabajando bajo el entorno MERN. Hace al rededor de un año estoy aprendiendo sobre AWS / GCP. Me inclino más al desarrollo front, también cuento con conocimientos básicos de UI/UX. También tengo +1 año de experiencia en desarrollo de apps mobiles (ReactNative),  he colaborado en varios proyectos que al día de hoy se encuentran en producción.',
    img: DavidImg,
    linkedin: 'https://www.linkedin.com/in/deveeup/',
    skills: ['Fullstack', 'Javascript', 'NodeJS', 'MERN'],
  },
};
export * from './validations';
export * from './roles';
export * from './typesMaterials';
