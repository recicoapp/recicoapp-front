import { validationMessages } from './messages';

export * from './messages';

export const passwordValidation = ({ value }) => {
  if (typeof value === 'string')
    return value.length < 6 ? validationMessages.min.string(6) : false;
};
