export const validationMessages = {
  empty: 'Este campo no puede estar vacío',
  min: {
    string: (min) => `Este campo debe contener al menos ${min} caracteres`,
  },
};
