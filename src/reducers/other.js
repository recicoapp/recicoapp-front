const initialState = {
  test: 'test other',
};
export default function (state = initialState, action) {
  switch (action.type) {
    case 'ADD_DATA_OTHER':
      return {
        ...state,
        ...action.payload,
      };
    case 'REMOVE_DATA_OTHER':
      return {
        ...initialState,
      };
    default:
      return state;
  }
}
