const initialState = {
  session: false,
  test: 'test var',
};
export default function (state = initialState, action) {
  switch (action.type) {
    case 'ADD_DATA_USER':
      return {
        ...state,
        ...action.payload,
      };
    case 'REMOVE_DATA_USER':
      return {
        ...initialState,
      };
    default:
      return state;
  }
}
