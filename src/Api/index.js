import Axios from 'axios';
const { REACT_APP_API = '' } = process.env;

if (!REACT_APP_API) {
  console.error('process.env.REACT_APP_API not defined');
}

const getUrl = (url) => {
  const newUrl = url.replace(/(\/+)/, '');
  return REACT_APP_API.replace(/(\/+)$/, '') + '/' + newUrl;
};
export class Api {
  static get(url, config) {
    return Axios.get(getUrl(url), config);
  }
  static delete(url, config) {
    return Axios.delete(getUrl(url), config);
  }

  static post(url, data, config) {
    return Axios.post(getUrl(url), data, config);
  }
  static put(url, data, config) {
    return Axios.put(getUrl(url), data, config);
  }
}

export default Api;
