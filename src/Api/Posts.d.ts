import { Roles } from '../constants';
export interface AllPost {
  '/user': {
    nickName: string;
    firstName: string;
    lastName: string;
    password: string;
    role: keyof typeof Roles;
  };
  '/login': {
    nickName: string;
    password: string;
  };
}
