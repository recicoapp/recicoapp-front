import { AxiosRequestConfig, AxiosResponse } from 'axios';

import { AllPost } from './Posts';

interface ApiBase {
  get<T = any, R = AxiosResponse<T>>(
    url: string,
    config?: AxiosRequestConfig,
  ): Promise<R>;

  post<T = any, R = AxiosResponse<T>, Url extends keyof AllPost = any>(
    url: Url,
    data?: AllPost[Url],
    config?: AxiosRequestConfig,
  ): Promise<R>;

  delete<T = any, R = AxiosResponse<T>>(
    url: string,
    config?: AxiosRequestConfig,
  ): Promise<R>;

  put<T = any, R = AxiosResponse<T>>(
    url: string,
    data?: any,
    config?: AxiosRequestConfig,
  ): Promise<R>;
}

export declare const Api: ApiBase;

export default Api;
