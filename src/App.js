import React from 'react';
import { HashRouter, Switch, Route, Redirect } from 'react-router-dom';

import LandingPage from './pages/LandingPage';
import Devpage from './pages/Members';
import ResultsPage from './pages/Results';
import HomeApp from './pages/App/Home';
import { PathRoutes } from './constants';
import Onboarding from './components/Onboarding';
import NoFound from './pages/404';
import { SessionProvider } from './components/Hooks/Session';

function App() {
  const onboarding = localStorage.getItem('renderOnboarding');
  return (
    <HashRouter>
      <SessionProvider>
        {!onboarding && <Onboarding />}
        <Switch>
          <Route path={PathRoutes.MEMBERS} component={Devpage} />
          <Redirect exact from={PathRoutes.INDEX} to={PathRoutes.MAIN} />
          <Route path={PathRoutes.MAIN} component={LandingPage} />
          <Route path={PathRoutes.APP} component={HomeApp} />
          <Route path={PathRoutes.RESULTS} component={ResultsPage} />
          <Route component={NoFound} />
        </Switch>
      </SessionProvider>
    </HashRouter>
  );
}

export default App;
