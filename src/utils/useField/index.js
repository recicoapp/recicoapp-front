import React from 'react';
/**
 *
 * @typedef {{ value: any; validate: boolean; validations?: ((field: Field)=>React.ReactNode[])[]} Field
 */
/**
 * @type {(
 *  props: Field
 * ) => ([{
 *  setValue(newValue: any): void;
 *  setField: React.SetStateAction<Record<Fields, Field>>;
 *  getValues: Record<Fields, any>;
 *  isValid(): boolean;
 *  disableValidation(): voids;
 *  activateValidation(): voids;
 *  valid: boolean;
 *  invalid: boolean;
 *  validate: boolean;
 *  errors?: React.ReactNode[]
 * } & Field, (newValue: any): void ]}
 */

export const useField = (props) => {
  const [, remount] = React.useState();
  const [field, setField] = React.useState({ validate: false, ...props });
  const reload = () => remount({});
  const setValue = (newValue) => {
    field.value = newValue;
    reload();
  };

  const getErrors = (breakOnError = false) => {
    const { validations } = field;
    const errors = [];
    if (validations) {
      for (const validation of validations) {
        const error = validation(field);
        if (!!error) {
          errors.push(error);
          if (breakOnError) break;
        }
      }
    }
    return errors;
  };
  return [
    {
      setField,
      setValue,
      disableValidation() {
        field.validate = false;
        reload();
      },
      activateValidation() {
        field.validate = true;
        reload();
      },
      isValid() {
        this.activateValidation();
        const errors = getErrors(true);
        return !errors.length;
      },
      get valid() {
        const { validate } = field;
        if (!validate) return true;
        const errors = getErrors(true);
        return !errors.length;
      },
      get invalid() {
        const valid = this.valid;
        return !valid;
      },
      get validate() {
        return field.validate;
      },
      get value() {
        return field.value;
      },
      get errors() {
        return getErrors();
      },
    },
    setValue,
  ];
};
