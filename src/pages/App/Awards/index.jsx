import React from 'react';
import './index.scss';
import FirstImage from 'assets/img/wather-park.png';
import SecondImage from 'assets/img/karting-kids.png';
import ThreeImage from 'assets/img/food-kids.png';
const Awards = () => {
  return (
    <div className="AwardsContainer">
      <h1 className="AwardsContainer-title">¡Premios del torneo!</h1>
      <div className="AwardsContainer-itemsContainer">
        <div className="AwardsContainer-itemsContainer-first">
          <figure>
            <img src={FirstImage} alt="" />
          </figure>
          <span className="AwardsContainer-itemsContainer-first-title">1er Lugar</span>
          <span className="AwardsContainer-itemsContainer-first-award">
            Fin de semana en Pisci Lago
          </span>
          <p className="AwardsContainer-itemsContainer-first-description">
            Un fin de semana con todos los gastos pagos, incluido desayuno, almuerzo y
            cena. Entrada al parque para todos los integrantes del grupo.
          </p>
        </div>
        <div className="AwardsContainer-itemsContainer-second">
          <figure>
            <img src={SecondImage} alt="" />
          </figure>
          <span className="AwardsContainer-itemsContainer-first-title">2do Lugar</span>
          <span className="AwardsContainer-itemsContainer-first-award">
            Entradaa RappidCards
          </span>
          <p className="AwardsContainer-itemsContainer-first-description">
            Disfruta de una experiencia única en Mundo Aventura, donde tendrás entrada
            libre y vueltas ilimitadas en los rappicards. Valido para todos los
            integrantes del equipo ganador.
          </p>
        </div>
        <div className="AwardsContainer-itemsContainer-three">
          <figure>
            <img src={ThreeImage} alt="" />
          </figure>
          <span className="AwardsContainer-itemsContainer-first-title">3er Lugar</span>
          <span className="AwardsContainer-itemsContainer-first-award">
            Bono McDonalds
          </span>
          <p className="AwardsContainer-itemsContainer-first-description">
            Visita nuestras instalaciones, disfruta de nuestros parques. Acá podrás
            redimir un bono de 100.000 COP. Valido hasta Diciembre de 2020, Incluye un
            bono para cada participante del quipo
          </p>
        </div>
      </div>
      <div className="AwardsContainer-results">
        <p>
          Visualice
          <a href="#/results/name_tournament" target="_blank">
            aquí
          </a>
          la información en tiempo real de los resultados
        </p>
      </div>
    </div>
  );
};

export default Awards;
