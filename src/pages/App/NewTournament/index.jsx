import React from 'react';
import IconAwards from 'assets/icons/icon-awards.svg';
import './index.scss';
const NewTournament = () => {
  return (
    <div className="NewTournamentContainer">
      <h4 className="NewTournamentContainer-title">
        <img src={IconAwards} alt="" />
        Nueva competencia
      </h4>
      <form action="" className="NewTournamentContainer-form">
        <div className="NewTournamentContainer-form-container">
          <span>Datos básicos de la competencia</span>
          <label htmlFor="">Nombre</label>
          <input type="text" placeholder="Nombre de la competenci" />
          <label htmlFor="">Fecha inicio</label>
          <input type="date" placeholder="Initial date" />
          <label htmlFor="">Fecha fin</label>
          <input type="date" placeholder="End date" />
          <label htmlFor="">Imagen</label>
          <input type="file" placeholder="End date" />
        </div>
        <div className="NewTournamentContainer-form-container">
          <span>Residuos a reciclar</span>
          <input type="text" placeholder="this is placeholder" />
          <input type="text" placeholder="this is placeholder" />
          <input type="text" placeholder="this is placeholder" />
        </div>
        <div className="NewTournamentContainer-form-container">
          <span>Title form</span>
          <input type="text" placeholder="this is placeholder" />
          <input type="text" placeholder="this is placeholder" />
          <input type="text" placeholder="this is placeholder" />
        </div>
        <button className="NewTournamentContainer-form-send">Crear formulario</button>
      </form>
    </div>
  );
};

export default NewTournament;
