import React from 'react';
import { Form, InputGroup, Table } from 'react-bootstrap';

import './index.scss';
import { typesMaterials } from '../../../constants';
import { useField } from '../../../utils';

/**
 * @typedef {{
 *  typeMaterial: string,
 *  quantity: number }[]} State
 */
export const RecordRecyclingAmount = () => {
  /**
   * @type {[State, Dispatch<SetStateAction<State>>]}
   */
  const [amounts, setAmounts] = React.useState([]);
  const [typeMaterial, setTypeMaterial] = useField({
    value: '',
    validations: [({ value }) => !value],
  });
  const [quantity, setQuantity] = useField({
    value: '',
    validations: [({ value }) => !value, ({ value }) => Number(value) >= 100],
  });

  return (
    <div className="RecordRecyclingAmount">
      <h4 className="RecordRecyclingAmount-title">
        Ingresa el peso de material reciclado
      </h4>
      <form action="" className="RecordRecyclingAmount-form">
        <div className="RecordRecyclingAmount-form-container font-weight-light">
          <Form.Label htmlFor="">Id del torneo</Form.Label>
          <Form.Control type="text" placeholder="Id del torneo" />
        </div>
        {(!!amounts.length && (
          <div className="RecordRecyclingAmount-form-container font-weight-light">
            <Table striped bordered hover>
              <thead>
                <tr className="font-weight-light h6">
                  <th>#</th>
                  <th>Tipo de material</th>
                  <th>Cantidad reciclada</th>
                </tr>
              </thead>
              <tbody>
                {amounts.map((amount, i) => {
                  const { label } = typesMaterials.find(
                    ({ id }) => id === amount.typeMaterial,
                  );
                  return (
                    <tr key={i}>
                      <td>{i + 1}</td>
                      <td>{label}</td>
                      <td>{amount.quantity}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </div>
        )) || <div className="p-4 my-4 text-center h6">Sin datos</div>}
        <div className="RecordRecyclingAmount-form-container font-weight-light">
          <Form.Label>Seleccione el tipo de material</Form.Label>
          <Form.Control
            as="select"
            value={typeMaterial.value}
            onChange={({ target }) => {
              setTypeMaterial(target.value);
            }}
          >
            {typesMaterials
              .filter(
                (typesMaterial) =>
                  !amounts.find((amount) => typesMaterial.id === amount.typeMaterial),
              )
              .map(({ id, label }) => {
                return (
                  <option key={id} value={id}>
                    {label}
                  </option>
                );
              })}
          </Form.Control>
          <Form.Label htmlFor="">Cantidad reciclada</Form.Label>
          <InputGroup>
            <Form.Control
              type="number"
              value={quantity.value}
              placeholder="Ingrese la cantidad reciclada"
              onChange={({ target }) => {
                setQuantity(target.value);
              }}
            />
            <InputGroup.Prepend>
              <InputGroup.Text id="inputGroupPrepend">Kg</InputGroup.Text>
            </InputGroup.Prepend>
          </InputGroup>
        </div>
        <button
          className="RecordRecyclingAmount-form-send"
          onClick={() => {
            const fields = [typeMaterial, quantity];
            if (fields.filter((field) => field.isValid()).length === fields.length) {
              /**
               * @type {State}
               */
              const newValue = [
                ...amounts,
                {
                  typeMaterial: typeMaterial.value,
                  quantity: quantity.value,
                },
              ];
              setAmounts(newValue);
            }
          }}
        >
          Agregar
        </button>
      </form>
    </div>
  );
};
