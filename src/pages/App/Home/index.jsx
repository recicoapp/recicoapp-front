import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Link } from 'react-router-dom';

import { PathRoutes, Roles } from '../../../constants';
import NewTournamentPage from '../NewTournament';
import NewTeamPage from '../NewTeam';
import AwardsPage from '../Awards';
import LayoutApp from 'components/LayoutApp';
import IconHomeApp from 'assets/icons/icon-recico-home-app.svg';
import { useSession, withAuthorization } from '../../../components/Hooks';

import './index.scss';
import { RecordRecyclingAmount } from '../RecordRecyclingAmount';

const HomeApp = () => {
  const { user } = useSession();
  return (
    <LayoutApp>
      <Switch>
        <Route path={`${PathRoutes.APP}/newTournament`} component={NewTournamentPage} />
        <Route path={`${PathRoutes.APP}/newTeam`} component={NewTeamPage} />
        <Route path={`${PathRoutes.APP}/awards`} component={AwardsPage} />
        <Route
          path={PathRoutes.RECORD_RECYCLING_AMOUNT}
          component={RecordRecyclingAmount}
        />
        <Route
          path={PathRoutes.APP}
          exact
          render={() => {
            return (
              <div className="HomeContainer">
                <h1 className="HomeContainer-title">
                  Bienvenido {user.firstName} {user.lastName}
                </h1>
                <div className="HomeContainer-welcome">
                  <img src={IconHomeApp} alt="" />
                  <p>
                    Con reciclando app, vamos a crear conciencia mientras nos
                    divertimos...
                  </p>
                </div>
                {user.role === Roles.ORGANIZER && (
                  <Link className="HomeContainer-newTournament" to="/app/newTournament">
                    Crear una nueva compentencia
                  </Link>
                )}

                {user.role === Roles.LEADER && (
                  <Link className="HomeContainer-addData" to="/app/addData">
                    Agregar datos de reciclaje
                  </Link>
                )}
              </div>
            );
          }}
        />
      </Switch>
    </LayoutApp>
  );
};

export default withAuthorization()(HomeApp);
