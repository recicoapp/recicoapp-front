import React from 'react';
import IconTeam from 'assets/icons/icon-team.svg';

const NewTeam = () => {
  return (
    <div className="NewTournamentContainer">
      <h4 className="NewTournamentContainer-title">
        <img src={IconTeam} alt="" />
        Crear equipo
      </h4>
      <form action="" className="NewTournamentContainer-form">
        <div className="NewTournamentContainer-form-container">
          <span>Datos de la competencia</span>
          <label htmlFor="">Clave única</label>
          <input type="text" placeholder="Id de la competencia" />
        </div>
        <div className="NewTournamentContainer-form-container">
          <span>Datos del equipo</span>
          <label htmlFor="">Nombre</label>
          <input type="text" placeholder="Nombre del equipo" />
          <label htmlFor="">Imagen</label>
          <input type="file" placeholder="Image" />
        </div>
        <div className="NewTournamentContainer-form-container">
          <span>Datos del lider (acceso)</span>
          <label htmlFor="">Usuario</label>
          <input type="text" placeholder="Usuario - Lider" />
          <label htmlFor="">Contraseña</label>
          <input type="password" placeholder="Contraseña" />
          <label htmlFor="">Confirmar contraseá</label>
          <input type="password" placeholder="Confirmar contraseña" />
        </div>

        <button className="NewTournamentContainer-form-send">Agregar equipo</button>
      </form>
    </div>
  );
};

export default NewTeam;
