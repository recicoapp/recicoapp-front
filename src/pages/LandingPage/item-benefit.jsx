import React from 'react';

const ItemBenefit = ({ item }) => (
  <div className="Benefits-item" id="benefits">
    <img src={item.img} alt="" />
    <div className="Benefits-item-container">
      <h4>{item.title}</h4>
      <p>{item.text}...</p>
    </div>
  </div>
);

export default ItemBenefit;
