import React from 'react';
import { Row, Col } from 'react-bootstrap';
import ItemBenefit from './item-benefit';
import RecyclingFamily from 'assets/icons/RecyclingFamily.svg';

import Awards from './awards';

import NaturalImg from 'assets/img/natural.jpg';
import CashImg from 'assets/img/cash.jpg';
import PetroleumImg from 'assets/img/petroleum.jpg';
import EnergyImg from 'assets/img/energy.jpg';
export const Items = () => (
  <React.Fragment>
    <Row className="mt-5">
      <Col className="align-items-center d-flex font-weight-normal h3" xs="12">
        ¿Qué es reciclar?
      </Col>
      <Col className="font-weight-light h6" xs="12">
        <Row className="align-items-center">
          <Col xs={12} md={7}>
            <p>
              Se entiende por reciclar la acción de convertir materiales de desecho en
              materia prima o en otros productos, de modo de extender su vida útil y
              combatir la acumulación de desechos en el mundo
              <br />
              El reciclaje reinserta el material de descarte de numerosas actividades
              industriales, empresariales o del consumo cotidiano, en la cadena
              productiva, permitiendo que sea reutilizado y disminuyendo la necesidad de
              adquirir o elaborar materiales nuevos
            </p>
          </Col>
          <Col xs>
            <img className="w-100 img-recycling-family" src={RecyclingFamily} alt="" />
          </Col>
        </Row>
      </Col>
    </Row>
    <div className="Benefits">
      <h2 className="Benefits-title">Beneficios de reciclar</h2>
      <ItemBenefit
        item={{
          title: 'Ahorra recursos naturales',
          img: NaturalImg,
          text:
            'Cada vez que reutilizas unproducto, ahorras la misma cantidad de material que se necesitaría para fabricar uno nuevo',
        }}
      />
      <ItemBenefit
        item={{
          title: 'Ahorro de energía',
          img: EnergyImg,
          text:
            'Fabricar un producto nuevo significa empezar de ceroel proceso de producción,lo que conlleva un importanteconsumo de energía',
        }}
      />
      <ItemBenefit
        item={{
          title: 'Disminuye la dependencia del petroleo',
          img: PetroleumImg,
          text:
            'El petróleo es el ingrediente principal de la industria de lafabricación de plástico, por lo que el uso del plástico recicladoconserva este recurso no renovable',
        }}
      />
      <ItemBenefit
        item={{
          title: 'Ahorra dinero y crea empleos',
          img: CashImg,
          text:
            'La industria del reciclado es un sector beneficioso para la economía, ya que genera multitud de empleos',
        }}
      />
    </div>
    <Awards />
    <p className="bigText">
      Recico, un app donde podrás ayudar al planeta mientras te diviertes...
      <br />
      <span>¡Súmate y haz parte el cambio!</span>
    </p>
  </React.Fragment>
);
