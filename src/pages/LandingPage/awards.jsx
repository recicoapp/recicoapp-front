import React from 'react';
import IconAwards from 'assets/icons/icon-awards.svg';
import IconAwards2 from 'assets/icons/icon-awrads-2.svg';
import IconAwards3 from 'assets/icons/icon-awards-3.svg';
const awards = () => {
  return (
    <div className="Awards">
      <h3 className="Awards-title">¡Personaliza los premios para cada commpetencia!</h3>
      <div className="Awards-itemContainer">
        <div className="Awards-itemContainer-item smallIcon">
          <img src={IconAwards2} alt="" />
          <span>@AdrianaGomez</span>
          <p>Bono Tiendas KOAJ (200.000 COP)</p>
        </div>
        <div className="Awards-itemContainer-item">
          <img src={IconAwards} alt="" />
          <span>@PabloRamirez</span>
          <p>
            Bono Falabella <br />
            (500.000 COP)
          </p>
        </div>
        <div className="Awards-itemContainer-item smallIcon">
          <img src={IconAwards3} alt="" />
          <span>@JuanGarzon</span>
          <p>
            Bono McDonals <br />
            (100.000 COP)
          </p>
        </div>
      </div>
    </div>
  );
};

export default awards;
