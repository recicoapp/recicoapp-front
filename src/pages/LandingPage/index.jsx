import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';

import Layout from 'components/Layout';
import Login from 'components/Login';
import SignUp from 'components/SignUp';
import Planet from 'assets/icons/Planet';
import { Items } from './items';
import './styles.scss';

export function LandingPage() {
  const [showLogin, setShowLogin] = React.useState(false);
  const [showSignUp, setShowSignUp] = React.useState(false);
  return (
    <Layout>
      <Login show={showLogin} onClose={() => setShowLogin(false)} />
      <SignUp show={showSignUp} onClose={() => setShowSignUp(false)} />
      <Container>
        <Row className="mt-2 firstComponent">
          <Col xs={12} md="8">
            <Row>
              <Col
                className="align-items-center d-flex font-weight-light h4 firstComponent-text"
                xs="8"
                md="8"
              >
                Nuestro objetivo es cuidar y hacer mejor el lugar donde vivimos...
              </Col>
              <Col xs="4" md="4">
                <div className="d-flex align-items-center h-100 ">
                  <Planet style={{ height: '180px' }} />
                </div>
              </Col>
            </Row>
          </Col>
          <Col xs={12} md="4">
            <Row className="bg-secondary bg-md-unset pb-4 mt-4">
              <Col className="resume d-md-none" xs="12">
                <p>
                  Con <b>Recico</b> puedes ayudar al planeta mientras te diviertes...
                  Organiza competencias, establece premios e incentiva el reiclaje.
                  <span>¡Juntos crearemos cultura!</span>
                </p>
              </Col>
              <Col xs="12">
                <Row className="pb-4 mt-4 align-items-center">
                  <Col xs={12}>
                    <div className="m-1 buttonContainer">
                      <Button
                        className="w-100"
                        onClick={() => {
                          setShowSignUp(true);
                        }}
                      >
                        Crear Cuenta
                      </Button>
                    </div>
                  </Col>
                  <Col xs={12}>
                    <div className="m-1 buttonContainer">
                      <Button
                        className="w-100 btn-register"
                        onClick={() => {
                          setShowLogin(true);
                        }}
                      >
                        Iniciar sesión
                      </Button>
                    </div>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Items />
      </Container>
    </Layout>
  );
}

export default LandingPage;
