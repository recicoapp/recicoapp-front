import React from 'react';
import { Row, Col } from 'react-bootstrap';

import Caneca from '../../../assets/icons/Caneca';
import './styles.scss';

/**
 *  @type {{ color: string, materials: string[] }[]}
 */
const canecas = [
  { color: '#fff', materials: ['Plástico', 'Cartón', 'Vidrio', 'Papel', 'Metales'] },
  { color: '#77ac60', materials: ['Restos de comida', 'Desechos', 'agrigolas'] },
  {
    color: '#585958',
    materials: [
      'Papel higíenico',
      'Servilletas',
      'Papeles y cartones contaminados con comida',
      'Pepeles Metalizados',
    ],
  },
];

export const Canecas = () => (
  <Row className="bg-secondary">
    <Col xs={12} className="align-items-center d-flex font-weight-normal h4 py-2">
      El reciclaje en Colombia
    </Col>
    <Col xs={12}>
      <div className="font-weight-light h6">
        A partir del 1 de enero de 2021, los únicos colores permitidos para el reciclaje
        serán el verde para los residuos orgánicos aprovechables, el blanco para residuos
        aprovechables y el negro para residuos no aprovechables, de la siguiente manera:
      </div>
      <Row>
        {canecas.map((caneca, i) => {
          const materials = caneca.materials.map((material) => material.toLowerCase());
          const haveTwo = materials.length > 1;
          let text = caneca.materials
            .slice(0, materials.length - (haveTwo ? 1 : 0))
            .map((material) => material.toLowerCase());
          text = haveTwo ? `y ${materials[materials.length - 1]}` : '';
          text += '.';
          return (
            <Col key={i} xs={12} sm={6} md={4} className="my-2">
              <Row className="h-100">
                <Col xs="auto" className="d-flex align-content-center">
                  <Caneca className="icon-caneca" color={caneca.color} />
                </Col>
                <Col xs>
                  <h6 className="font-weight-light">
                    Para depositar los residuos aprovechables como {text}
                  </h6>
                </Col>
              </Row>
            </Col>
          );
        })}
      </Row>
    </Col>
  </Row>
);

export default Canecas;
