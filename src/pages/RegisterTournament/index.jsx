import React from 'react';
import { useForm, useStep } from "react-hooks-helper";
import FirstStep from '../../components/RegisterTournament/formFirstStep'
import './styles.scss';

function RegisterTournament() {

  return (
    <div className="div-container">
      <FirstStep />
    </div>
  );
}

export default RegisterTournament;
