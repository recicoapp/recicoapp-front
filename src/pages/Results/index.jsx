import React from 'react';
import { Link } from 'react-router-dom';
import './index.scss';
import NumberOne from 'assets/icons/icon-awards.svg';
import NumberTwo from 'assets/icons/icon-awrads-2.svg';
import NumberThree from 'assets/icons/icon-awards-3.svg';
const ResultsPage = () => {
  return (
    <div className="ResultsContainer">
      <h3>Tabla de posiciones de la competencia @name_tournament</h3>
      <div className="ResultsContainer-results">
        <div className="ResultsContainer-results-item">
          <figure>
            <img src={NumberOne} alt="" />
          </figure>
          <div className="ResultsContainer-results-item-info">
            <span className="ResultsContainer-results-item-info-position">
              1er. Lugar
            </span>
            <span className="ResultsContainer-results-item-info-team">
              @AvengersTeamTest
            </span>
            <p className="ResultsContainer-results-item-info-award">
              Bono 500.000 COP en Falabella
            </p>
            <span className="ResultsContainer-results-item-info-recycle">
              25kg en desechos solidos
            </span>
          </div>
        </div>
        <div className="ResultsContainer-results-item">
          <figure>
            <img src={NumberTwo} alt="" />
          </figure>
          <div className="ResultsContainer-results-item-info">
            <span className="ResultsContainer-results-item-info-position">
              2do. Lugar
            </span>
            <span className="ResultsContainer-results-item-info-team">
              @SupporTeamTest
            </span>
            <p className="ResultsContainer-results-item-info-award">
              Bono 400.000 COP en McDonals
            </p>
            <span className="ResultsContainer-results-item-info-recycle">
              18kg en cartón
            </span>
          </div>
        </div>
        <div className="ResultsContainer-results-item">
          <figure>
            <img src={NumberThree} alt="" />
          </figure>
          <div className="ResultsContainer-results-item-info">
            <span className="ResultsContainer-results-item-info-position">
              3er. Lugar
            </span>
            <span className="ResultsContainer-results-item-info-team">
              @GlobantHackathon2020
            </span>
            <p className="ResultsContainer-results-item-info-award">
              Bono 100.000 COP en Claro
            </p>
            <span className="ResultsContainer-results-item-info-recycle">
              25kg en desechos orgánicos
            </span>
          </div>
        </div>
      </div>
      <Link to="/home">Ir a recicoapp.com</Link>
    </div>
  );
};

export default ResultsPage;
