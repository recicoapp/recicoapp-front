import React from 'react';
import { Link } from 'react-router-dom';
import Layout from 'components/Layout';
import NotFoundLogo from 'assets/icons/no-found.svg';
import './index.scss';
const NoFound = () => {
  return (
    <Layout>
      <div className="NotFoundContainer">
        <img src={NotFoundLogo} alt="" />
        <h4>¡UPS! No hemos encontrado lo que buscas...</h4>
        <p>
          Regresar al <Link to="/home">Inicio</Link>
        </p>
      </div>
    </Layout>
  );
};

export default NoFound;
