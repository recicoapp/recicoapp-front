/* eslint-disable react/jsx-key */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable no-shadow */
import React from 'react';
import Layout from 'components/Layout';
import { MEMBERS } from '../../constants';
import './index.scss';
/**
 * @type {React.ComponentType<import('react-router-dom').RouteComponentProps<{dev:string}>}
 */
const DevPage = ({ location, match }) => {
  const pathDev = match.params.dev;
  let developer = location.state;
  if (!developer) {
    developer = {
      name: 'nose',
    };
    const findObject = Object.keys(MEMBERS).find((member) => pathDev === member);
    if (!findObject) {
      console.log('not found');
    } else {
      developer = MEMBERS[findObject];
    }
  }
  return (
    <Layout>
      <div className="ContainerDev">
        <img src={developer.img} alt="" />
        <h3>@{developer.name}</h3>
        <div className="ContainerDev-skills">
          {developer.skills.map((skill) => (
            <span>{skill}</span>
          ))}
        </div>
        <p>{developer.resume}</p>
        <div className="ContainerDev-social">
          <a href={developer.github} target="_blank">
            Github
          </a>
          <a href={developer.linkedin} target="_blank">
            LinkedIn
          </a>
        </div>
      </div>
    </Layout>
  );
};

export default DevPage;
