export const addDataOther = (payload) => {
  return {
    type: 'ADD_DATA_OTHER',
    payload,
  };
};
export const removeDataOther = () => {
  return {
    type: 'REMOVE_DATA_OTHER',
  };
};
