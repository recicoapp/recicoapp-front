import React from 'react';
import Menu from './menu';
const LayoutApp = ({ children }) => {
  return (
    <div className="BodyApp">
      {children}
      <Menu />
    </div>
  );
};

export default LayoutApp;
