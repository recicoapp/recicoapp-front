import React from 'react';
import { useSession } from 'components/Hooks/Session';
import { Link } from 'react-router-dom';
import './index.scss';
import Home from 'assets/icons/icon-home.svg';
import Tournament from 'assets/icons/icon-tournament.svg';
import LogoutIcon from 'assets/icons/icon-logout.svg';
import User from 'assets/icons/icon-user.svg';
const Menu = () => {
  const { closeSession } = useSession();
  return (
    <footer className="MenuApp">
      <Link to="/app" className="MenuApp-item">
        <img src={Home} alt="" />
      </Link>
      <Link to="/app/awards" className="MenuApp-item">
        <img src={Tournament} alt="" />
      </Link>
      <Link to="/app/newTeam" className="MenuApp-item">
        <img src={User} alt="" />
      </Link>
      <a
        href="/"
        className="MenuApp-item"
        onClick={(e) => {
          e.preventDefault();
          closeSession();
        }}
      >
        <img src={LogoutIcon} alt="" />
      </a>
    </footer>
  );
};

export default Menu;
