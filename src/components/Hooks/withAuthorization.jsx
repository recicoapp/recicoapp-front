import React from 'react';
import { Redirect } from 'react-router-dom';

import { PathRoutes } from '../../constants';
import { useSession } from './Session';
/**
 * @typedef { (state: import('./Session').UserState) => void | undefined } ConditionFn
 */

const Authorization = ({ component: Component, componentProps, conditionFn }) => {
  const { token, user } = useSession();
  if (!token || !user || (conditionFn && conditionFn({ token, user }))) {
    return <Redirect to={PathRoutes.INDEX} />;
  }
  return <Component {...componentProps} />;
};
/**
 * @param {ConditionFn} conditionFn
 */
export const withAuthorization = (conditionFn) => {
  return (WrappedComponent) => (props) => (
    <Authorization
      {...{ component: WrappedComponent, componentProps: props, conditionFn }}
    />
  );
};
