import React from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';

/**
 * @typedef {{
 *  deletedAt: null;
 *  firstName: string;
 *  lastName: string;
 *  nickName: string;
 *  role: string;
 *  status: boolean;
 *  __v: number;
 *  _id: string;
 * }} User
 */
/**
 * @typedef {{
 *   token: string | null
 *   user: User | null
 * }} UserState
 */

/**
 * @type {UserState}
 */

/**
 * @type {React.Context<UserState & {
 *  setSession: React.Dispatch<React.SetStateAction<UserState>;
 *  closeSession: (state:UserState) => void };
 * };
 * }
 */
export const SessionContext = React.createContext(Object.create(null));

const keySession = 'session';
const initialValue = { token: null, user: null };

const getInitialValue = () => {
  let value = localStorage.getItem(keySession);
  if (value) {
    try {
      value = JSON.parse(value);
    } catch (e) {
      value = {};
    }
  }
  return { ...initialValue, ...value };
};

/**
 *
 * @param {React.PropsWithChildren<{}>} param0
 */
export const SessionProvider = ({ children }) => {
  /**
   * @type {[UserState,React.Dispatch<React.SetStateAction<UserState>>]}
   */
  const [session, setSession] = React.useState(() => getInitialValue());
  const setSessionValue = (newState) => {
    const state = { ...session, ...newState };
    localStorage.setItem(keySession, JSON.stringify(state));
    setSession({ ...session, ...newState });
  };
  const history = useHistory();

  return (
    <SessionContext.Provider
      value={{
        ...session,
        setSession: (newState) => {
          setSessionValue(newState);
        },
        closeSession: () => {
          setSessionValue({ ...initialValue });
          localStorage.removeItem(keySession);
          history.push('/home');
        },
      }}
    >
      {children}
    </SessionContext.Provider>
  );
};
export const SessionConsumer = () => ({ children }) => {
  return (
    <SessionContext.Consumer>
      {(value) => {
        if (!value) return null;
        return children(value);
      }}
    </SessionContext.Consumer>
  );
};

SessionConsumer.propTypes = {
  children: PropTypes.func.isRequired,
};
export const useSession = () => React.useContext(SessionContext);
