import React from 'react';

import './item.scss';
const ScreenItem = ({ icon, title }) => {
  return (
    <div className="Item">
      <img src={icon} alt="" className="Item-icon" />
      <div className="Item-container">
        <h2>{title}</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam nesciunt aut
          expedita nulla dignissimos. Qui beatae eum tempore quae, magnam porro, hic
          veritatis modi laudantium, vero aut quisquam repudiandae odio! Lorem ipsum dolor
          sit amet consectetur adipisicing elit. Quisquam nesciunt aut expedita nulla
          dignissimos.
        </p>
      </div>
    </div>
  );
};

export default ScreenItem;
