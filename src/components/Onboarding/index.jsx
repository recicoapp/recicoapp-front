import React, { useState } from 'react';
import { Modal, Button, Carousel } from 'react-bootstrap';
import ScreenItem from './item';
import ImgOne from 'assets/img/icon-one.svg';
import ImgTwo from 'assets/img/icon-two.svg';
import ImgThree from 'assets/img/icon-three.svg';
import './index.scss';
export const Onboarding = () => {
  localStorage.setItem('renderOnboarding', true);
  const [show, setShow] = useState(true);
  const handleClose = () => setShow(false);

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        size="xl"
        animation
        dialogClassName="PersonalizedModal"
      >
        <Modal.Body className="Body">
          <Carousel indicators={false}>
            <Carousel.Item>
              <ScreenItem icon={ImgOne} title="Title one" />
            </Carousel.Item>
            <Carousel.Item>
              <ScreenItem icon={ImgTwo} title="Title two" />
            </Carousel.Item>
            <Carousel.Item>
              <ScreenItem icon={ImgThree} title="Title three" />
            </Carousel.Item>
          </Carousel>
        </Modal.Body>
        <Modal.Footer className="Footer">
          <Button onClick={handleClose} className="Footer-button">
            Finalizar introducción
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default Onboarding;
