import React, { useState } from 'react';
import { Container, Row, Col, Form, Card, Button } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export default function FormFirstStep() {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [name, setName] = useState();
  const [files, setFile] = useState();

  const handleSubmit = (evt) => {
    evt.preventDefault();

    const dataSend = {
      name: name,
      date_start: startDate,
      date_end: endDate,
      img: files,
      category: {
        a: 'b',
        b: 'd',
      },
    };
    console.log(dataSend);
  };

  const getBase64 = (file, cb) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
     reader.onload = function () {
        cb(reader.result)
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
  }
  return (
    <div>
      <Container fluid>
        <h3>Nueva competencia</h3>
        <Col>
          <Form onSubmit={handleSubmit}>
            <Card>
              <Card.Body>
                <Card.Title>Informacion Competencia</Card.Title>
                <Row>
                  <Col sm>
                    <Form.Label>Nombre*</Form.Label>
                    <Form.Control
                      type="text"
                      name="name"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                    />
                    <Form.Text className="text-muted"></Form.Text>{' '}
                  </Col>
                  <Col sm>
                    <Form.Label>Fecha Inicio</Form.Label>
                    <DatePicker
                      className="form-control"
                      selected={startDate}
                      onChange={(date) => setStartDate(date)}
                      selectsStart
                      startDate={startDate}
                      endDate={endDate}
                      name="dateStart"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col sm>
                    <Form.Label>Fecha Fin</Form.Label>
                    <DatePicker
                      className="form-control"
                      selected={endDate}
                      onChange={(date) => setEndDate(date)}
                      selectsEnd
                      startDate={startDate}
                      endDate={endDate}
                      minDate={startDate}
                    />
                  </Col>
                  <Col sm>
                    <Form.File id="formcheck-api-regular">
                      <Form.File.Label >Imagen*</Form.File.Label>
                      <Form.File.Input onChange={(e) => getBase64(e.target.files[0], (result)=> {
                        setFile(result);                  
                      })}/>
                    </Form.File>
                  </Col>
                </Row>
              </Card.Body>
            </Card>
            <Card>
              <Card.Body>
                <Card.Title>Parametros de la Competencia</Card.Title>
                <Row>
                  <Col>
                    <Form.Label>U. de medida</Form.Label>
                    <Form.Control as="select">
                      <option value="1">Kg</option>
                      <option value="2">Lbs</option>
                      <option value="3">Vol</option>
                    </Form.Control>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Label>Que tipo de residuo vas a medir?</Form.Label>
                    <Form.Check
                      type="switch"
                      id="green"
                      label="Residuos ordinarios, no reciclables"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Check type="switch" id="grey" label="Cartón y papel" />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Check type="switch" id="blue" label="Plásticos" />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Check type="switch" id="white" label="Vidrio" />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Check type="switch" id="beige" label="Residuos orgánicos" />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Form.Check type="switch" id="yellow" label="Aluminio o metales" />
                  </Col>
                </Row>
              </Card.Body>
            </Card>
            <div className="m-1">
              <Button type="submit">Crear competencia</Button>
            </div>
          </Form>
        </Col>
      </Container>
    </div>
  );
}
