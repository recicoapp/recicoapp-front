import React from 'react';
import { Link } from 'react-router-dom';
import './index.scss';
import FB from 'assets/icons/icon-facebook.svg';
import IG from 'assets/icons/icon-instagram.svg';
import { MEMBERS } from '../../constants';
const Footer = () => {
  return (
    <div className="FooterLanding">
      <h5>Recico</h5>
      <span>Ayudemos al medio ambiente mientras nos divertimos.</span>
      <div className="FooterLanding-socialMedia">
        <div className="FooterLanding-socialMedia-item">
          <img src={FB} alt="" />
          <a href="https://facebook.com/recicoapp" target="#">
            /recicoapp
          </a>
        </div>
        <div className="FooterLanding-socialMedia-item">
          <img src={IG} alt="" />
          <a href="https://instagram.com/recicoapp" target="#">
            @recicoapp
          </a>
        </div>
      </div>
      <div className="FooterLanding-about">
        <p>Realizado por</p>
        <div className="FooterLanding-about-members">
          <Link
            to={{
              pathname: MEMBERS.kevin.url,
              state: MEMBERS.kevin,
            }}
          >
            @Kevin
          </Link>
          <Link
            to={{
              pathname: MEMBERS.eduard.url,
              state: MEMBERS.eduard,
            }}
          >
            @Eduard
          </Link>
          <Link
            to={{
              pathname: MEMBERS.leonardo.url,
              state: MEMBERS.leonardo,
            }}
          >
            @Leonardo
          </Link>
          <Link
            to={{
              pathname: MEMBERS.david.url,
              state: MEMBERS.david,
            }}
          >
            @David
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Footer;
