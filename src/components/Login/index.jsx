import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import isEmpty from 'validator/lib/isEmpty';
import iconRegister from 'assets/icons/icon-register.svg';
import logo from 'assets/icons/logo.svg';

import { Modal, Button, Form } from 'react-bootstrap';
import { useField } from '../../utils';

import './index.scss';
import Api from '../../Api';
import { validationMessages, passwordValidation, PathRoutes } from '../../constants';
import { useSession } from '../Hooks/Session';

/**
 *
 * @param {{ show: boolean, onClose: () => void }} props
 */
export const Login = (props) => {
  const { show, onClose } = props;
  const { setSession } = useSession();
  const history = useHistory();

  const [username, setUsername] = useField({
    value: '',
    validations: [({ value }) => (isEmpty(value) ? validationMessages.empty : false)],
  });
  const [password, setPassword] = useField({
    value: '',
    validations: [
      ({ value }) => (isEmpty(value) ? validationMessages.empty : false),
      passwordValidation,
    ],
  });

  const getValues = () => ({ username: username.value, password: password.value });

  const handleClose = () => {
    onClose();
  };
  const getErrors = (field) =>
    field.errors &&
    field.errors.map((error, i) => (
      <Form.Control.Feedback type="invalid" key={i}>
        {error}
      </Form.Control.Feedback>
    ));

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        size="xl"
        animation
        centered
        className="maxsize"
      >
        <Modal.Body className="LoginContainer">
          <img src={iconRegister} alt="" />
          <div className="LoginContainer-recico">
            <div>
              <img src={logo} alt="" />
              <span>
                Bienvenido a <b>Recico</b>
              </span>
            </div>
            <p>¡Juntos vamos a contribuir con el reciclaje!</p>
          </div>
          <Form>
            <Form.Group>
              <Form.Control
                value={username.value}
                isInvalid={username.invalid}
                onChange={({ target }) => {
                  setUsername(target.value);
                }}
                type="text"
                placeholder="Usuario"
              />
              {getErrors(username)}
            </Form.Group>
            <Form.Group>
              <Form.Control
                value={password.value}
                isInvalid={password.invalid}
                onChange={({ target }) => {
                  setPassword(target.value);
                }}
                type="password"
                placeholder="Contraseña"
              />
              {getErrors(password)}
            </Form.Group>
            <div className="w-100 d-flex justify-content-center">
              <Button
                variant="primary"
                onClick={() => {
                  const fields = [username, password];
                  if (
                    fields.filter((field) => field.isValid()).length === fields.length
                  ) {
                    const values = getValues();
                    Api.post('/login', {
                      nickName: values.username,
                      password: values.password,
                    })
                      .then((res) => {
                        const { token, user } = res.data;
                        setSession({ token, user });
                        history.push(PathRoutes.APP);
                      })
                      .catch((e) => console.log(e));
                  }
                }}
              >
                Iniciar sesión
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};
Login.prototype = {
  show: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};
export default Login;
