import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import Logo from 'assets/icons/logo.svg';
import './index.scss';

export const Header = () => (
  <Navbar bg="light" expand="lg" className="Header">
    <div className="Header-container">
      <Navbar.Brand href="#home">
        <img src={Logo} className="Header-container-logo" alt="" />
        Recico
      </Navbar.Brand>
    </div>
    <Navbar.Toggle />
    <Navbar.Collapse id="basic-navbar-nav">
      <div className="mr-auto" />
      <Nav>
        <Nav.Link href="#home">Inicio</Nav.Link>
        <Nav.Link href="#benefits" onClick={(e) => e.preventDefault()}>
          Beneficios
        </Nav.Link>
        <Nav.Link href="#home">Iniciar sesión</Nav.Link>
        <Nav.Link href="#home">Registrate</Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

export default Header;
