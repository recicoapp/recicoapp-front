import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'validator/lib/isEmpty';
import logo from 'assets/icons/logo.svg';
import iconSignUp from 'assets/icons/icon-user-signup.svg';

import { Modal, Button, Form } from 'react-bootstrap';
import { useField } from '../../utils';
import './index.scss';
import { validationMessages, passwordValidation, Roles } from '../../constants';
import Api from '../../Api';

/**
 *
 * @param {{ show: boolean, onClose: () => void }} props
 */
export const SignUp = (props) => {
  const { show, onClose } = props;
  const fieldEmpty = ({ value }) => (isEmpty(value) ? validationMessages.empty : false);
  const [username, setUsername] = useField({
    value: '',
    validations: [fieldEmpty],
  });
  const [name, setName] = useField({
    value: '',
    validations: [fieldEmpty],
  });
  const [lastName, setLastName] = useField({
    value: '',
    validations: [fieldEmpty],
  });
  const [password, setPassword] = useField({
    value: '',
    validations: [fieldEmpty, passwordValidation],
  });
  const [passwordConfirmation, setPasswordConfirmation] = useField({
    value: '',
    validations: [
      ({ value }) => {
        if (value !== password.value) return 'La constrasela no es igual';
      },
      fieldEmpty,
      passwordValidation,
    ],
  });
  const getValues = () => ({
    username: username.value,
    name: name.value,
    lastName: lastName.value,
    password: password.value,
  });

  const handleClose = () => {
    onClose();
  };

  const getErrors = (field) =>
    field.errors &&
    field.errors.map((error, i) => (
      <Form.Control.Feedback type="invalid" key={i}>
        {error}
      </Form.Control.Feedback>
    ));

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        size="xl"
        animation
        centered
        className="maxsize"
      >
        <Modal.Body className="SignupContainer">
          <img src={iconSignUp} alt="" />
          <div className="SignupContainer-recico">
            <div>
              <img src={logo} alt="" />
              <span>
                Bienvenido a <b>Recico</b>
              </span>
            </div>
            <p>¡Excelente decisión! Gracias por formar parte de Recico.</p>
          </div>
          <Form>
            <Form.Group>
              <Form.Control
                value={username.value}
                isInvalid={username.invalid}
                onChange={({ target }) => {
                  setUsername(target.value);
                }}
                type="text"
                placeholder="Ingrese su usuario"
              />
              {getErrors(username)}
            </Form.Group>
            <Form.Group>
              <Form.Control
                value={name.value}
                isInvalid={name.invalid}
                onChange={({ target }) => {
                  setName(target.value);
                }}
                type="text"
                placeholder="Ingrese su nombre"
              />
              {getErrors(name)}
            </Form.Group>
            <Form.Group>
              <Form.Control
                value={lastName.value}
                isInvalid={lastName.invalid}
                onChange={({ target }) => {
                  setLastName(target.value);
                }}
                type="text"
                placeholder="Ingrese su apellido"
              />
              {getErrors(lastName)}
            </Form.Group>
            <Form.Group>
              <Form.Control
                value={password.value}
                isInvalid={password.invalid}
                onChange={({ target }) => {
                  setPassword(target.value);
                }}
                type="password"
                placeholder="Contraseña"
              />
              {getErrors(password)}
            </Form.Group>
            <Form.Group>
              <Form.Control
                value={passwordConfirmation.value}
                isInvalid={passwordConfirmation.invalid}
                onChange={({ target }) => {
                  setPasswordConfirmation(target.value);
                }}
                type="password"
                placeholder="Confirmar contraseña"
              />
              {getErrors(passwordConfirmation)}
            </Form.Group>
            <div className="w-100 d-flex justify-content-center">
              <Button
                variant="primary"
                onClick={() => {
                  const fields = [
                    username,
                    name,
                    lastName,
                    password,
                    passwordConfirmation,
                  ];
                  if (
                    fields.filter((field) => field.isValid()).length === fields.length
                  ) {
                    const values = getValues();

                    Api.post('/user', {
                      nickName: values.username,
                      firstName: values.name,
                      lastName: values.lastName,
                      password: values.password,
                      role: Roles.ORGANIZER,
                    });
                  }
                  handleClose();
                }}
              >
                Crear cuenta
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};
SignUp.prototype = {
  show: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};
export default SignUp;
